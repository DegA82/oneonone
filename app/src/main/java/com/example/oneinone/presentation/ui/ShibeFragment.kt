package com.example.oneinone.presentation.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.oneinone.data.ShibeRepo
import com.example.oneinone.databinding.FragmentShibeBinding
import com.example.oneinone.presentation.adapter.ShibeAdapter
import com.example.oneinone.presentation.viewmodel.ShibeViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ShibeFragment: Fragment() {

    private var _binding: FragmentShibeBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var repo: ShibeRepo
    private val shibeViewModel by viewModels<ShibeViewModel> {
        ShibeViewModel.ShibeViewModelFactory(repo)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentShibeBinding.inflate(inflater, container, false).also { _binding =it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        shibeViewModel.state.observe(viewLifecycleOwner){ state ->
            with(binding){
                rvShibe.adapter = ShibeAdapter(state.shibes)
            }

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}