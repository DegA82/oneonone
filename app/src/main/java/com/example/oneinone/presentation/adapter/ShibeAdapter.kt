package com.example.oneinone.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import com.example.oneinone.data.local.entity.Shibe
import com.example.oneinone.databinding.ItemShibeBinding

class ShibeAdapter(
    private val shibes: List<Shibe>
): RecyclerView.Adapter<ShibeAdapter.MyViewHolder>() {


    class MyViewHolder(
        private var binding: ItemShibeBinding

    ):RecyclerView.ViewHolder(binding.root) {

        fun loadImage(shibe: Shibe){
            with(binding){
                ivShibe.load(shibe.url){
                    transformations(CircleCropTransformation())
                }
            }
        }

        companion object{
            fun getInstance(parent:ViewGroup)=ItemShibeBinding.inflate(
                LayoutInflater.from(parent.context),parent,false
            ).let { binding ->MyViewHolder(binding) }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int
    ) = MyViewHolder.getInstance(parent)

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val url = shibes[position]
        holder.loadImage(url)
    }

    override fun getItemCount(): Int {
        return shibes.size
    }
}