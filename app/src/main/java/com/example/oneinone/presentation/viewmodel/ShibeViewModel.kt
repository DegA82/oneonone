package com.example.oneinone.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.example.oneinone.data.ShibeRepo
import com.example.oneinone.data.local.entity.Shibe
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ShibeViewModel @Inject constructor(
    private val shibeRepo: ShibeRepo
) :ViewModel() {

    val state = liveData<ShibeState> {
        emit(ShibeState(isLoading = true))
        val shibes = shibeRepo.getShibe()
        emit(ShibeState(shibes = shibes))
    }

    data class ShibeState(
        val isLoading: Boolean = false,
        val shibes: List<Shibe> = emptyList()
    )

    class ShibeViewModelFactory(
        private val shibeRepo: ShibeRepo
    ): ViewModelProvider.NewInstanceFactory(){

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ShibeViewModel(shibeRepo) as T
        }
    }
}