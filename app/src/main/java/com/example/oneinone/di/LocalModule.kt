package com.example.oneinone.di

import android.content.Context
import androidx.room.Room
import com.example.oneinone.data.local.ShibeDatabase
import com.example.oneinone.data.local.dao.ShibeDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object LocalModule {

    @Provides
    fun providersShibeDatabase(
        @ApplicationContext context:Context
    ): ShibeDatabase{
        return Room.databaseBuilder(context, ShibeDatabase::class.java, "shibe.db"
        ).build()
    }

    @Provides
    fun providesShibeDao(db:ShibeDatabase): ShibeDao{ //Dao has connection with Database so it talk
        return db.shibeDao()
    }
}