package com.example.oneinone.di

import com.example.oneinone.data.remote.ShibeService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

@Module
@InstallIn(SingletonComponent::class)
object RemoteModule {

    @Provides
    //To get the instance of retrofit object
    fun providesRetrofit(): Retrofit{
        return Retrofit.Builder()
            .baseUrl("https://shibe.online")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    //here we pass the service and call the retrofit instance obtained from retrofit
    fun providesShibeService(retrofit: Retrofit): ShibeService{
        return retrofit.create()
    }

}