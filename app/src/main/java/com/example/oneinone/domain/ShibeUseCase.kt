package com.example.oneinone.domain

import com.example.oneinone.data.ShibeRepo
import com.example.oneinone.data.local.entity.Shibe
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception
import javax.inject.Inject

class ShibeUseCase @Inject constructor(private val shibeRepo: ShibeRepo) {

    suspend operator fun invoke(): Result<List<Shibe>> = withContext(Dispatchers.IO){
        return@withContext try {
            val shibeResponse = shibeRepo.getShibe()
            Result.success(shibeResponse)
        } catch (ex:Exception){
            Result.failure(ex)
        }
    }
}