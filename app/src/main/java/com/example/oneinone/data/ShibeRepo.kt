package com.example.oneinone.data

import android.util.Log
import com.example.oneinone.data.local.dao.ShibeDao
import com.example.oneinone.data.local.entity.Shibe
import com.example.oneinone.data.remote.ShibeService
import javax.inject.Inject

private const val TAG = "ShibeRepo"

class ShibeRepo @Inject constructor(
    private val shibeService: ShibeService,
    private val shibeDao: ShibeDao) {

    suspend fun getShibe(): List<Shibe>{

        val urls: List<String> = shibeService.getShibes()
        val shibes: List<Shibe> = urls.map{ url-> Shibe(url=url) }
        shibeDao.insertAll(shibes)
        Log.d(TAG, "${shibeDao.getAllShibes().size}" )
        return shibes

    }

}