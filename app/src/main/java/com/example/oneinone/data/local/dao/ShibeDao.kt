package com.example.oneinone.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.oneinone.data.local.entity.Shibe

@Dao
interface ShibeDao {

    @Query("SELECT * FROM shibe")
    suspend fun getAllShibes(): List<Shibe>

    @Insert
    suspend fun insertAll(shibe: List<Shibe>)
}