package com.example.oneinone.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.oneinone.data.local.dao.ShibeDao
import com.example.oneinone.data.local.entity.Shibe

@Database(entities = [Shibe::class], version = 1)
abstract class ShibeDatabase: RoomDatabase() {
    abstract fun shibeDao(): ShibeDao
    
}